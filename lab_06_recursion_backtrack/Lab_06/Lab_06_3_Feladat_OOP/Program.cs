﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_06_3_Feladat_OOP
{
    // ==============================================================================================================
    // 
    // Feladat:
    // Implementáld a Backtrack(), az Ft() és Fk() metódusokat, hogy az órai példához hasonlóan működjön a program!
    // A különbség, hogy itt sztringek helyett Dolgozo objektumok reprezentálják a feladatokhoz beosztható embereket.
    // 
    // ==============================================================================================================


    class Program
    {
        static void Main(string[] args)
        {
            int N = 6;
            Dolgozo[,] R = new Dolgozo[N, 3];

            // feladatkör 1
            R[0, 0] = new Dolgozo("Peter");
            R[0, 1] = new Dolgozo("Lois");

            // feladatkör 2
            R[1, 0] = new Dolgozo("Chris");
            R[1, 1] = new Dolgozo("Peter");

            // feladatkör 3
            R[2, 0] = new Dolgozo("Meg");

            // feladatkör 4
            R[3, 0] = new Dolgozo("Meg");
            R[3, 1] = new Dolgozo("Stewie");
            R[3, 2] = new Dolgozo("Chris");

            // feladtkör 5
            R[4, 0] = new Dolgozo("Meg");
            R[4, 1] = new Dolgozo("Brian");

            // felaatkör 6
            R[5, 0] = new Dolgozo("Brian");
            R[5, 1] = new Dolgozo("Peter");

            int[] M = new int[] { 2, 2, 1, 3, 2, 2 };
            
            Dolgozo[] E = new Dolgozo[N];

            Feladatkioszto kioszto = new Feladatkioszto(N, R, M);

            if (kioszto.MegoldasKereses(E))
            {
                for (int i = 0; i < E.Length; i++)
                {
                    Console.WriteLine("Feladatkör " + (i + 1) + ": " + E[i]);
                }
            }
            else
            {
                Console.WriteLine("Nincs megoldás");
            }

            Console.ReadLine();
        }
    }
}
