﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_06_3_Feladat_OOP
{
    abstract class BacktrackBase
    {
        int N;
        Dolgozo[,] R;
        int[] M;

        public BacktrackBase(int n, Dolgozo[,] r, int[] m)
        {
            N = n;
            R = r;
            M = m;
        }

        protected abstract bool Ft(int szint, Dolgozo dolgozo);
        protected abstract bool Fk(int szint, Dolgozo dolgozo, Dolgozo[] E);

        void Backtrack(int szint, Dolgozo[] E, ref bool van)
        {
            // TODO
        }

        public bool MegoldasKereses(Dolgozo[] E)
        {
            bool van = false;
            Backtrack(0, E, ref van);
            return van;
        }
    }
}
