﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_06_3_Feladat_OOP
{
    class Dolgozo
    {
        public string Nev { get; private set; } // ez igazából lehetne privát is, mivel összehasonlításhoz használhatjuk az Equals(), kiíratáshoz meg a ToString() metódusokat

        public Dolgozo(string nev)
        {
            Nev = nev;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Dolgozo))
                return false;

            return this.Nev == (obj as Dolgozo).Nev;
        }

        public override int GetHashCode()
        {
            return Nev.GetHashCode();
        }

        public override string ToString()
        {
            return Nev;
        }
    }
}
