﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_06_2_Feladat
{
    // ===============================================================================
    // 
    // Változók jelentése:
    // - N      :   megoldandó részproblémák ( itt: feladatkörök) száma
    // - R      :   lehetséges részmegoldások tömbje
    // - M      :   a lehetséges részmegoldások darabszámai az egyes részproblémákhoz
    // - szint  :   éppen melyik (hanyadik) részfeladatot próbáljuk megoldani
    // - van    :   a megoldáskeresés sikerességét jelzi
    // - E      :   a (választott) részmegoldásokat tartalmazó tömb
    //
    // Ft(szint, r):
    // - a szint-edik részfeladat esetében lehetséges megoldás-e az r
    // - "lokális"
    //
    // Fk(szint, r, E):
    // - a szint-edik részfeladat esetében választhatjuk-e az r részmegoldást, amennyiben az előző szinteken az E tömbben található részmegoldásokat választottuk
    // - "globális"
    // 
    // ===============================================================================


    class Program
    {
        static void Main(string[] args)
        {
            int N = 6;
            string[,] R = new string[N, 3];

            // feladatkör 1
            R[0, 0] = "Peter";
            R[0, 1] = "Lois";

            // feladatkör 2
            R[1, 0] = "Chris";
            R[1, 1] = "Peter";

            // feladatkör 3
            R[2, 0] = "Meg";

            // feladatkör 4
            R[3, 0] = "Meg";
            R[3, 1] = "Stewie";
            R[3, 2] = "Chris";

            // feladtkör 5
            R[4, 0] = "Meg";
            R[4, 1] = "Brian";

            // felaatkör 6
            R[5, 0] = "Brian";
            R[5, 1] = "Peter";

            int[] M = new int[] { 2, 2, 1, 3, 2, 2 };


            bool van = false;
            string[] E = new string[N];

            Backtrack(0, E, ref van, N, R, M);

            if (van)
            {
                for (int i = 0; i < E.Length; i++)
                {
                    Console.WriteLine("Feladatkör " + (i + 1) + ": " + E[i]);
                }
            }
            else
            {
                Console.WriteLine("Nincs megoldás");
            }

            Console.ReadLine();
        }

        static bool Ft(int szint, string nev)
        {
            return true;
        }

        static bool Fk(int szint, string nev, string[] E)
        {
            for (int i = 0; i < szint; i++)
            {
                if (E[i] == nev)
                    return false;
            }
            return true;
        }

        static void Backtrack(int szint, string[] E, ref bool van, int N, string[,] R, int[] M)
        {
            int i = -1;
            while (!van && i < M[szint] - 1)
            {
                i++;
                if (Ft(szint, R[szint, i]))
                {
                    if (Fk(szint, R[szint, i], E))
                    {
                        E[szint] = R[szint, i];
                        if (szint == N - 1)
                        {
                            van = true;
                        }
                        else
                        {
                            Backtrack(szint + 1, E, ref van, N, R, M);
                        }
                    }
                }
            }
        }
    }
}
