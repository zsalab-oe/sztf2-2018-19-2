﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_04_4_Kivetel_Hallgato
{
    class NeptunkodExcpetion : Exception
    {
        // kivételtípus létrehozásánál érdemes elmenteni néhány - a kivételkezelés szempontjából hasznos - adatot, ezzel némi információt szolgáltatva majd a kivételt elkapónak / kezelőnek
        // tipikusan ilyen a kivételt okozó (problémás) érték, vagy objektum(ra egy referencia)


        string neptunkod;

        public string Neptunkod { get { return neptunkod; } }

        public NeptunkodExcpetion(string neptunkod)
        {
            this.neptunkod = neptunkod;
        }

        public override string Message
        {
            get { return "Túl hosszú vagy túl rövid a megadott neptunkód."; }
        }
    }

    class KisbetusNeptunkodException : NeptunkodExcpetion
    {
        public KisbetusNeptunkodException(string neptunkod)
            : base(neptunkod)
        {

        }

        public override string Message
        {
            get { return "A megadott neptunkódban (" +
                    this.Neptunkod + 
                    ") nem szerepelhet kisbetű."; }
        }
    }
}
