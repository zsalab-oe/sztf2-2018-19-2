﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_04_4_Kivetel_Hallgato
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Hallgato bela = HallgatoLetrehozas();
                Console.WriteLine("hallgató sikeresen létrehozva");
            }
            catch (KisbetusNeptunkodException e) // az 'e' referenciaváltozóval tudunk hivatkozni az eldobott kivételobjektumra, így kinyerhető belőle a hibaüzenet és egyéb elmentett adat
            {
                Console.WriteLine(e.Message + " " + DateTime.Now);
            }
            catch (NeptunkodExcpetion e)
            {
                Console.WriteLine("HIBA - " + e.Message + " Problémás neptunkód: " + e.Neptunkod + ". Hiba időpont: " + DateTime.Now);
            }



            Console.ReadLine();
        }

        static Hallgato HallgatoLetrehozas()
        {
            Console.Write("Add meg a neptunkódot: ");
            string neptunkod = Console.ReadLine();

            if (neptunkod.Length != 6)
                throw new NeptunkodExcpetion(neptunkod);

            if (KisbetuEldontes(neptunkod))
                throw new KisbetusNeptunkodException(neptunkod);

            return new Hallgato(neptunkod);
        }

        static bool KisbetuEldontes(string neptunkod)
        {
            int j = 0;
            while (j < neptunkod.Length &&
                (!char.IsLower(neptunkod[j]))) // IsDigit() ellenőrzés nem is kell :)
            {
                j++;
            }
            return j < neptunkod.Length;
        }
    }
}
