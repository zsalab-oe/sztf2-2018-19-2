﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_04_2_Kivetel_Tombkezeles
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                TombKezeles();
            }
            catch (FormatException e)
            {
                Console.WriteLine("HIBA - nem megfelelő formátum (számjegyeket írj).");
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine("HIBA - kiindexeltél a tömbből.");
            }
            finally
            {
                Console.WriteLine("Ez mindig lefut.");
            }

            Console.WriteLine("\n-------------------------\n");

            SzamKezeles();

            Console.ReadLine();
        }

        static void TombKezeles()
        {
            Console.WriteLine("Add meg a tömb méretét!");
            int[] tomb = new int[int.Parse(Console.ReadLine())];
            Console.WriteLine("Add meg a tömb elemeit!");
            for (int i = 0; i < tomb.Length; i++)
            {
                Console.Write((i + 1) + ".: ");
                tomb[i] = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Hanyadik elemre vagy kíváncsi?");
            Console.WriteLine(tomb[int.Parse(Console.ReadLine())]);
        }

        static void SzamKezeles() // TryParse() példa
        {
            Console.Write("TryParse() példa - Adj meg egy számot: ");
            int szam;
            if (int.TryParse(Console.ReadLine(), out szam))
                Console.WriteLine("Szám = " + szam);
            else
                Console.WriteLine("Nem sikerült :(");

            /* int.TryParse():
             * - megpróbálja átalakítani a string-et számmá
             * - a szám címszerinti paraméterátadással kap értéket
             * - ha sikerült átalakítani (a string egy számot tartalmazott), igazat ad vissza, és közben a szám értéke a string-ben lévő érték lesz
             * - ha nem sikerült, akkor NEM száll el a program FormatException hibával (ellentétben az int.Parse() függvénnyel), csak hamisat ad vissza, és közben a szám értéke 0 lesz */
        }
    }
}
