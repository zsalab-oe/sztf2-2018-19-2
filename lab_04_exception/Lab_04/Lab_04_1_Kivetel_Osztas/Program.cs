﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_04_1_Kivetel_Osztas
{
    class NullavalOsztasException : Exception // kötelezően az Exception osztály leszármazottja (enélkül nem lehet "hibát dobni")
    {

    }

    class MasikException : Exception { }

    class Program
    {
        static void Main(string[] args)
        {
            try // a problémás kódrészeket try blokkba tesszük
            {
                Console.WriteLine(Osztas(5, 0));
                Console.WriteLine("további utasítások...");
            }
            catch (MasikException) // ide nem fogunk beugrani, mert ilyen típusú kivétel nem adódhat a try blokkban
            {
                Console.WriteLine("Ismeretlen hiba.");
            }
            catch (NullavalOsztasException) // itt kezeljük le a kivételt
            {
                Console.WriteLine("HIBA - 0-val osztás.");
            }
            catch (Exception e) // ide nem fogunk beugrani, mert egy korábbi catch blokkban már lekezeltük a hibát
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.WriteLine("Ez mindig lefut.");
            }

            Console.ReadLine();
        }

        static int Osztas(int osztando, int oszto)
        {
            if (oszto == 0)
                throw new NullavalOsztasException(); // új kivételOBJEKTUM eldobása

            return osztando / oszto;
        }
    }
}
