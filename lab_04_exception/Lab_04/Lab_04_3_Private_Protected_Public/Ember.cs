﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_04_3_Private_Protected_Public
{
    class Ember
    {
        protected string nev;
        protected int eletkor;

        public string Nev { get { return nev; } }

        public Ember(string nev, int eletkor)
        {
            this.nev = nev;
            this.eletkor = eletkor;
        }

        void EmberMetodus()
        {
            this.eletkor = 4; // elérhető
        }

        protected void OrokoltMetodus()
        {
            // TODO
        }
    }
}
