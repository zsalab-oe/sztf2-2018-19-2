﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_04_3_Private_Protected_Public
{
    class Hallgato : Ember
    {
        public Hallgato(string nev, int eletkor)
            : base(nev, eletkor)
        {

        }

        void Metodus()
        {
            eletkor = 10; // örökölt mező, leszármazott osztályban elérhető láthatósági szinttel
            nev = "Józsi";
            OrokoltMetodus();
        }
    }
}
