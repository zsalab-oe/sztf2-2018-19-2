﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_04_3_Private_Protected_Public
{
    class Program
    {
        static void Main(string[] args)
        {
            Hallgato h = new Hallgato("Pista", 20);
            //int kor = h.eletkor; // Hibás, innen nem érjük el
            //h.OrokoltMetodus(); // szintén
            Console.WriteLine(h.Nev); // innen csak a publikus tulajdonságot érjük el, a (védett) mezőt nem!


            // ================================================================
            // Összefoglalás
            // - private:   csak az adott osztályból elérhető
            // - protected: adott osztályból és leszármazottaiból érhető el
            // - public:    bármelyik osztályból elérhető
        }
    }
}
