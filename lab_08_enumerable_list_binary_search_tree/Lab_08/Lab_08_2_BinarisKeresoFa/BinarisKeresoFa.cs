﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_08_2_BinarisKeresoFa
{
    class BinarisKeresoFa<T, K> where K : IComparable
    {
        class FaElem
        {
            public T tartalom;
            public K kulcs;
            public FaElem bal, jobb;

            public FaElem(T tartalom, K kulcs)
            {
                this.tartalom = tartalom;
                this.kulcs = kulcs;
                bal = null;
                jobb = null;
            }
        }

        FaElem gyoker;

        public BinarisKeresoFa()
        {
            gyoker = null;
        }

        public void Beszuras(T tartalom, K kulcs)
        {
            Beszuras(ref gyoker, tartalom, kulcs);
        }

        void Beszuras(ref FaElem p, T tartalom, K kulcs)
        {
            if (p == null)
            {
                p = new FaElem(tartalom, kulcs);
            }
            else
            {
                if (p.kulcs.CompareTo(kulcs) > 0)
                    Beszuras(ref p.bal, tartalom, kulcs);
                else
                    Beszuras(ref p.jobb, tartalom, kulcs);
            }
        }

        void Feldolgoz(T tartalom)
        {
            // csinálunk valamit
            Console.WriteLine(tartalom.ToString());
        }

        public void Bejaras()
        {
            //PreOrder(gyoker);
            InOrder(gyoker);
        }

        void PreOrder(FaElem p)
        {
            if (p != null)
            {
                Feldolgoz(p.tartalom);
                PreOrder(p.bal);
                PreOrder(p.jobb);
            }
        }

        void InOrder(FaElem p)
        {
            if (p != null)
            {
                InOrder(p.bal);
                Feldolgoz(p.tartalom);
                InOrder(p.jobb);
            }
        }
    }
}
