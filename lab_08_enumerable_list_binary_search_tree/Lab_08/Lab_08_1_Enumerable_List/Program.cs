﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_08_1_Enumerable_List
{
    class Program
    {
        static void Main(string[] args)
        {
            LancoltLista<Auto> lista = new LancoltLista<Auto>();
            lista.BeszurasElejere(new Auto() { Rendszam = "ABC-123" });
            lista.BeszurasElejere(new Auto() { Rendszam = "SOR-444" });
            lista.BeszurasVegere(new Auto() { Rendszam = "BOR-555" });
            lista.BeszurasAdottHelyre(new Auto() { Rendszam = "UFO-456" }, 2);

            try
            {
                lista.Torles(new Auto() { Rendszam = "SOR-444" });
            }
            catch (NincsIlyenElemException e)
            {
                Console.WriteLine("HIBA - " + e.Message);
                //Program.Kiir("HIBA - " + e.Message);
            }

            lista.Bejaras();



            // ===================================================================

            // Tegyél töréspontokat a ListaBejaro osztály két Current tulajdonságához, és F11-gyel kövesd végig, melyik esetben melyik tulajdonság illetve GetEnumerator() metódus kerül meghívásra!

            // ===================================================================


            // foreach
            Console.WriteLine("\n============== foreach ==============\n");
            foreach (var a in lista)
                Console.WriteLine(a.Rendszam);

            
            // IEnumarator<T> használata
            Console.WriteLine("\n============== IEnumarator<T> ==============\n");
            IEnumerator<Auto> tmp = lista.GetEnumerator();
            while (tmp.MoveNext())
                Console.WriteLine(tmp.Current.Rendszam);


            // IEnumarator használata
            Console.WriteLine("\n============== IEnumarator ==============\n");
            IEnumerator tmp2 = lista.GetEnumerator();
            while (tmp2.MoveNext())
                Console.WriteLine(tmp2.Current);


            // IEnumarator használata v2
            Console.WriteLine("\n============== IEnumarator ==============\n");
            IEnumerable bejarhatoSzerkezet = lista;
            IEnumerator tmp3 = bejarhatoSzerkezet.GetEnumerator();
            while (tmp3.MoveNext())
                Console.WriteLine(tmp3.Current);


            // foreach v2
            Console.WriteLine("\n============== foreach v2 ==============\n");
            IEnumerable bejarhato = lista;
            foreach (var item in bejarhato)
                Console.WriteLine(item);



            Console.ReadLine();
        }

        static void Kiir(string s)
        { }
    }
}
