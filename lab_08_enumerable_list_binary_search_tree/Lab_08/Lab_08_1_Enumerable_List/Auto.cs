﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_08_1_Enumerable_List
{
    class Auto
    {
        public string Rendszam { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Auto))
                return false;

            return this.Rendszam.Equals((obj as Auto).Rendszam);
        }

        public override int GetHashCode()
        {
            return Rendszam.GetHashCode();
        }

        public override string ToString()
        {
            return Rendszam;
        }
    }
}
