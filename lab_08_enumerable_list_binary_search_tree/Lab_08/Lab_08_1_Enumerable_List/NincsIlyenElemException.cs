﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_08_1_Enumerable_List
{
    class NincsIlyenElemException : Exception
    {
        public NincsIlyenElemException() { }
        public NincsIlyenElemException(string message) : base(message) { }
    }
}
