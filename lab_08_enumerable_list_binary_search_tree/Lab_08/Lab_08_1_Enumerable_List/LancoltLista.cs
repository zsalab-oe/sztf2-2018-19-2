﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_08_1_Enumerable_List
{
    class LancoltLista<T> : IEnumerable<T> // System.Collections.Generic névtérben található
    {
        class ListaElem
        {
            public T tartalom;
            public ListaElem kovetkezo;
        }

        class ListaBejaro : IEnumerator<T> // System.Collections.Generic névtérben található
        {
            ListaElem elso;         // a lista első elemére mutat
            ListaElem jelenlegi;    // a bejáró ezzel a hivatkozással járja végig a listaelemeket

            public ListaBejaro(ListaElem elso)
            {
                this.elso = elso;
                jelenlegi = null;
            }


            #region IEnumerator<T> megvalósítása esetén ezek kerülnek be:

            public T Current { get { return jelenlegi.tartalom; } }

            object IEnumerator.Current { get { return this.Current; } }

            public void Dispose() // erőforrások felszabadítására szolgál (esetünkben hagyhatnánk üresen is), a bejárás végeztével automatikusan meghívódik
            {
                elso = null;
                jelenlegi = null;
            }

            public bool MoveNext() // A következő elemre próbál lépni. Igazat ad vissza, ha van ilyen, különben pedig hamisat.
            {
                if (jelenlegi == null) // első hívás
                    jelenlegi = elso;
                else // n. hívás (n > 1)
                    jelenlegi = jelenlegi.kovetkezo;

                return jelenlegi != null;
            }

            public void Reset() // alaphelyzetbe állítja a bejárót (első elem elé)
            {
                jelenlegi = null;
                //throw new NotSupportedException();
                // bővebb infó: https://docs.microsoft.com/en-us/dotnet/api/system.collections.ienumerator.reset?view=netframework-4.7.2#remarks
            }

            #endregion
        }

        ListaElem fej;

        public LancoltLista()
        {
            fej = null;
        }

        public void BeszurasElejere(T tartalom)
        {
            ListaElem uj = new ListaElem();
            uj.tartalom = tartalom;
            uj.kovetkezo = fej;
            fej = uj;
        }

        public void BeszurasVegere(T tartalom)
        {
            ListaElem uj = new ListaElem();
            uj.tartalom = tartalom;
            uj.kovetkezo = null;

            if (fej == null)
            {
                fej = uj;
            }
            else
            {
                ListaElem p = fej;
                while (p.kovetkezo != null)
                {
                    p = p.kovetkezo;
                }
                p.kovetkezo = uj;
            }
        }

        public void BeszurasAdottHelyre(T ertek, int n)
        {
            ListaElem uj = new ListaElem();
            uj.tartalom = ertek;
            if (fej == null || n == 1)
            {
                uj.kovetkezo = fej;
                fej = uj;
            }
            else
            {
                ListaElem p = fej;
                int i = 2;
                while (p.kovetkezo != null && i < n)
                {
                    p = p.kovetkezo;
                    i++;
                }
                uj.kovetkezo = p.kovetkezo;
                p.kovetkezo = uj;
            }
        }

        void Feldolgoz(T tartalom)
        {
            // csinálunk valamit
            Console.WriteLine(tartalom.ToString());
        }

        public void Bejaras()
        {
            ListaElem p = fej;
            while (p != null)
            {
                Feldolgoz(p.tartalom);
                p = p.kovetkezo;
            }
        }

        public void Torles(T torlendo)
        {
            ListaElem p = fej;
            ListaElem e = null;
            while (p != null && !p.tartalom.Equals(torlendo))
            {
                e = p;
                p = p.kovetkezo;
            }
            if (p != null)
            {
                if (e == null)                  // megvan (első elem)
                    fej = p.kovetkezo;
                else                            // megvan (többi elem között)
                    e.kovetkezo = p.kovetkezo;
                //p = null;
            }
            else                                // nem talált VAGY üres lista
            {
                throw new NincsIlyenElemException($"Nincs ilyen ({torlendo.ToString()}) elem a listában.");
            }
        }

        public void ListaTorles()
        {
            ListaElem p;
            while (fej != null)
            {
                p = fej;
                fej = fej.kovetkezo;
                //p = null;
            }
        }


        #region IEnumerable<T> megvalósítása esetén ezek kerülnek be:

        public IEnumerator<T> GetEnumerator()
        {
            return new ListaBejaro(fej);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        } 
        #endregion
    }
}
