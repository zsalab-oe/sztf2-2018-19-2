﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_09_1_Graf_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            //GrafCsucsMatrix g = new GrafCsucsMatrix(7);
            GrafSzomszedsagiLista g = new GrafSzomszedsagiLista(7);
            g.ElFelvetel(0, 1);
            g.ElFelvetel(1, 2);
            g.ElFelvetel(2, 5);
            g.ElFelvetel(0, 3);
            g.ElFelvetel(1, 3);
            g.ElFelvetel(5, 3);
            g.ElFelvetel(5, 6);
            g.ElFelvetel(0, 4);

            Console.WriteLine("Csúcsok:");
            WriteLine(g.Csucsok());
            Console.WriteLine();

            Console.WriteLine("Szomszédok(3):");
            WriteLine(g.Szomszedok(3));
            Console.WriteLine();

            Console.WriteLine("Szélességi bejárás:");
            g.SzelessegiBejaras(0);
            Console.WriteLine("\n");

            Console.WriteLine("Mélységi bejárás:");
            g.MelysegiBejaras(0);


            Console.ReadLine();
        }

        public static void WriteLine(List<int> l)
        {
            foreach (int x in l)
                Console.Write(x + " ");
            Console.WriteLine();
        }

        public static void Write(string s)
        {
            Console.Write(s + " ");
        }
    }
}
