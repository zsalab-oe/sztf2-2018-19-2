﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_09_1_Graf_Demo
{
    class GrafCsucsMatrix : Graf
    {
        int[,] CS;

        public GrafCsucsMatrix(int n) : base(n)
        {
            CS = new int[n, n];
        }

        public override List<int> Csucsok()
        {
            List<int> l = new List<int>();
            for (int i = 0; i < N; i++)
            {
                l.Add(i);
            }
            return l;
        }

        public override void ElFelvetel(int honnan, int hova)
        {
            // mivel irányítatlan a gráf, ezért mind a két "oldalról" jelezni kell a kapcsolatot
            CS[honnan, hova]++;
            CS[hova, honnan]++;
        }

        public override bool VezetEl(int honnan, int hova)
        {
            return CS[honnan, hova] != 0;
        }
    }
}
