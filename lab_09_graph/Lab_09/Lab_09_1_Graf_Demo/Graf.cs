﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_09_1_Graf_Demo
{
    abstract class Graf
    {
        protected int N; // csúcsok száma

        protected Graf(int n)
        {
            N = n;
        }

        public abstract List<int> Csucsok();
        public abstract void ElFelvetel(int honnan, int hova);
        public abstract bool VezetEl(int honnan, int hova);
        public List<int> Szomszedok(int csucs)
        {
            List<int> l = new List<int>();
            //for (int i = 0; i < N; i++)   // VAGY:
            foreach(int i in Csucsok())
            {
                if(VezetEl(csucs, i))
                {
                    l.Add(i);
                }
            }
            return l;
        }

        void Feldolgoz(int csucs)
        {
            Program.Write(csucs.ToString());
        }

        public void SzelessegiBejaras(int start)
        {
            //Enqueue(elem): elem berakása
            //Dequeue(): legkorábban berakott elem kivétele és törlése

            Queue<int> S = new Queue<int>();
            List<int> F = new List<int>();
            S.Enqueue(start);
            F.Add(start);
            while (S.Count != 0)
            {
                int k = S.Dequeue();
                Feldolgoz(k);
                foreach (int x in Szomszedok(k))
                {
                    if (!F.Contains(x)) // eldönti, hogy x benne van-e F-ben
                    {
                        S.Enqueue(x);
                        F.Add(x);
                    }
                }
            }
        }

        public void MelysegiBejaras(int start)
        {
            List<int> F = new List<int>();
            MelysegiBejaras(start, ref F);
        }

        void MelysegiBejaras(int k, ref List<int> F)
        {
            F.Add(k);
            Feldolgoz(k);
            foreach (int x in Szomszedok(k))
            {
                if (!F.Contains(x))
                {
                    MelysegiBejaras(x, ref F);
                }
            }
        }
    }
}
