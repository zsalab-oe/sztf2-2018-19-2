﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_09_2_Generikus_Graf
{
    class GrafCsucsMatrix<T> : Graf<T>
    {
        List<T> csucsok;
        int[,] CS;
        
        public GrafCsucsMatrix(int N) : base(N)
        {
            CS = new int[N, N];
            csucsok = new List<T>();
        }

        public void CsucsFelvetel(T uj)
        {
            csucsok.Add(uj);
        }

        public override List<T> Csucsok()
        {
            return csucsok;
        }

        public override void ElFelvetel(T honnan, T hova)
        {
            CS[csucsok.IndexOf(honnan), csucsok.IndexOf(hova)]++;
            CS[csucsok.IndexOf(hova), csucsok.IndexOf(honnan)]++;
        }

        public override bool VezetEl(T honnan, T hova)
        {
            return CS[csucsok.IndexOf(honnan), csucsok.IndexOf(hova)] != 0;
        }
    }
}