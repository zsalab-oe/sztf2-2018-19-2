﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_09_2_Generikus_Graf
{
    class GrafSzomszedsagiLista<T> : Graf<T>
    {
        List<T> csucsok;
        List<int>[] L;
        
        public GrafSzomszedsagiLista(int N) : base(N)
        {
            L = new List<int>[N];
            csucsok = new List<T>();
            for (int i = 0; i < N; i++)
            {
                L[i] = new List<int>();
            }
        }

        public void CsucsFelvetel(T uj)
        {
            csucsok.Add(uj);
        }

        public override List<T> Csucsok()
        {
            return csucsok;
        }

        public override void ElFelvetel(T honnan, T hova)
        {
            L[csucsok.IndexOf(honnan)].Add(csucsok.IndexOf(hova));
            L[csucsok.IndexOf(hova)].Add(csucsok.IndexOf(honnan));
        }

        public override bool VezetEl(T honnan, T hova)
        {
            return L[csucsok.IndexOf(honnan)].Contains(csucsok.IndexOf(hova));
        }
    }
}
