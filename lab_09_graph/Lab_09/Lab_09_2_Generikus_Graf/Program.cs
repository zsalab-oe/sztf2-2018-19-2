﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_09_2_Generikus_Graf
{
    class Program
    {
        static void Main(string[] args)
        {
            int N = 7;

            Auto a1 = new Auto("AAA-111");
            Auto a2 = new Auto("BBB-222");
            Auto a3 = new Auto("CCC-333");
            Auto a4 = new Auto("DDD-444");
            Auto a5 = new Auto("EEE-555");
            Auto a6 = new Auto("FFF-666");
            Auto a7 = new Auto("GGG-777");

            //GrafCsucsMatrix<Auto> g = new GrafCsucsMatrix<Auto>(N);
            GrafSzomszedsagiLista<Auto> g = new GrafSzomszedsagiLista<Auto>(N);

            g.CsucsFelvetel(a1);
            g.CsucsFelvetel(a2);
            g.CsucsFelvetel(a3);
            g.CsucsFelvetel(a4);
            g.CsucsFelvetel(a5);
            g.CsucsFelvetel(a6);
            g.CsucsFelvetel(a7);

            g.ElFelvetel(a1, a2);
            g.ElFelvetel(a1, a4);
            g.ElFelvetel(a1, a5);
            g.ElFelvetel(a2, a3);
            g.ElFelvetel(a2, a4);
            g.ElFelvetel(a3, a4);
            g.ElFelvetel(a3, a6);
            g.ElFelvetel(a4, a6);
            g.ElFelvetel(a6, a7);

            Console.WriteLine("Csúcsok listája:");
            WriteLine(g.Csucsok());
            Console.WriteLine();

            Console.WriteLine("0. csúcs szomszédja:");
            WriteLine(g.Szomszedok(a1));
            Console.WriteLine();

            Console.WriteLine("Szélességi bejárás:");
            g.SzelessegiBejaras(a1);


            Console.ReadLine();
        }

        public static void Write(string s)
        {
            Console.Write(s);
        }

        public static void WriteLine<T>(List<T> l)
        {
            foreach (T t in l)
                Console.Write(t.ToString() + " ");
            Console.WriteLine();
        }
    }
}
