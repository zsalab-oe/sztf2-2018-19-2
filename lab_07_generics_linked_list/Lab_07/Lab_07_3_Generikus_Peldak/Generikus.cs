﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_07_3_Generikus_Peldak
{
    class Tarolo<T> where T : class
    { /* ... */ }

    class LeszarmazottTarolo<T> : Tarolo<T> where T : class // megszorítások nem öröklődnek, azokat meg kell ismételni
    { }

    static class Eszkozok
    {
        public static T Kisebb<T>(T egyik, T masik) where T : IComparable
        {
            if (egyik.CompareTo(masik) < 0)
                return egyik;
            else
                return masik;

            // ugyanez rövidebben:
            //return egyik.CompareTo(masik) < 0 ? egyik : masik;
        }

        public static void Csere<T>(ref T egyik, ref T masik)
        {
            T seged = egyik;
            egyik = masik;
            masik = seged;
        }
    }
}
