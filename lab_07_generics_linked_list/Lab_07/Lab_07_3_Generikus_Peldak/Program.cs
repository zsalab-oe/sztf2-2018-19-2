﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_07_3_Generikus_Peldak
{
    class Ember
    {

    }

    class Program
    {
        static void Main(string[] args)
        {
            //Tarolo<int> tarolo1 = new Tarolo<int>(); // hibás, 'T' nem lehet érték típus
            Tarolo<Ember> tarolo2 = new Tarolo<Ember>();

            int x = 5;
            int y = 100;
            Console.WriteLine(x + ", " + y);
            //Eszkozok.Csere<int>(ref x, ref y);
            Eszkozok.Csere(ref x, ref y);
            Console.WriteLine(x + ", " + y);
        }
    }
}
