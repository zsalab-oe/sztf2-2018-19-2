﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_07_4_LancoltLista
{
    class Program
    {
        static void Main(string[] args)
        {
            LancoltLista<int> lista1 = new LancoltLista<int>();
            

            try
            {
                lista1.BeszurasElejere(10);
                lista1.BeszurasElejere(20);
                lista1.BeszurasVegere(35);
                lista1.BeszurasElejere(40);
                lista1.BeszurasVegere(55);

                lista1.Torles(40);
                lista1.Torles(123456);
            }
            catch (NincsIlyenElemException)
            {
                Console.WriteLine("Az elem törlése nem sikerült.");
            }

            lista1.Bejaras();

            Console.ReadLine();
        }
    }
}
