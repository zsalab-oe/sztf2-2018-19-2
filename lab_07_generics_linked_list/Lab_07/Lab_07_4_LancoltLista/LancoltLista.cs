﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_07_4_LancoltLista
{
    class NincsIlyenElemException : Exception { }

    class LancoltLista<T>
    {
        class ListaElem
        {
            public T tartalom;
            public ListaElem kovetkezo;
        }

        ListaElem fej;

        public LancoltLista()
        {
            fej = null;
        }

        public void BeszurasElejere(T tartalom)
        {
            ListaElem uj = new ListaElem();
            uj.tartalom = tartalom;
            uj.kovetkezo = fej;
            fej = uj;
        }

        public void BeszurasVegere(T tartalom)
        {
            ListaElem uj = new ListaElem();
            uj.tartalom = tartalom;
            uj.kovetkezo = null;

            if (fej == null)
            {
                fej = uj;
            }
            else
            {
                ListaElem p = fej;
                while (p.kovetkezo != null)
                {
                    p = p.kovetkezo;
                }
                p.kovetkezo = uj;
            }
        }

        void Feldolgoz(T tartalom)
        {
            // csinálunk valamit
            Console.WriteLine(tartalom.ToString());
        }

        public void Bejaras()
        {
            ListaElem p = fej;
            while (p != null)
            {
                Feldolgoz(p.tartalom);
                p = p.kovetkezo;
            }
        }

        public void Torles(T torlendo)
        {
            ListaElem p = fej;
            ListaElem e = null;
            while (p != null && !p.tartalom.Equals(torlendo))
            {
                e = p;
                p = p.kovetkezo;
            }
            if (p != null)
            {
                if (e == null)                  // megvan (első elem)
                    fej = p.kovetkezo;
                else                            // megvan (többi elem között)
                    e.kovetkezo = p.kovetkezo;
            }
            else                                // nem talált VAGY üres lista
            {
                throw new NincsIlyenElemException();
            }
        }
    }
}
