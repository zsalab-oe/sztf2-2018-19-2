﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_07_2_Generikus_Verem
{
    class Verem<T>
    {
        T[] elemek;
        int db;

        public Verem(int meret)
        {
            elemek = new T[meret];
            db = 0;
        }

        // új elem betétele a verem "tetejére"
        public void Push(T elem)
        {
            elemek[db++] = elem;
        }

        // elem kivétele a verem "tetejéről"
        public T Pop()
        {
            T vissza = elemek[--db];
            return vissza;
        }

        // megfelső elem "megnézése"
        public T Peek()
        {
            T vissza = elemek[db - 1];
            return vissza;
        }
    }
}
