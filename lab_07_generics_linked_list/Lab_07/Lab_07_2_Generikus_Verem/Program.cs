﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_07_2_Generikus_Verem
{
    class Ember
    {
        public string Nev { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Verem<int> v = new Verem<int>(5);

            // TODO: hibakezelés

            v.Push(20);
            v.Push(30);
            Console.WriteLine(v.Pop());
            Console.WriteLine(v.Peek());
            Console.WriteLine(v.Peek());
            Console.WriteLine(v.Peek());
            Console.WriteLine(v.Peek());
            Console.WriteLine(v.Pop());

            Console.WriteLine("====================");

            Verem<Ember> v2 = new Verem<Ember>(5);
            v2.Push(new Ember() { Nev = "András" });
            v2.Push(new Ember() { Nev = "Béla" });

            Console.WriteLine(v2.Pop().Nev);
            Console.WriteLine(v2.Pop().Nev);
        }
    }
}
