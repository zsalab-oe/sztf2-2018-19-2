﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_07_1_Verem
{
    class NincsTobbHelyException : Exception
    {
        public NincsTobbHelyException()
            : base("Tele a verem")
        { }
    }

    class UresVeremException : Exception
    {
        public UresVeremException()
            :base("Üres a verem")
        { }
    }

    class Verem
    {
        string[] elemek;
        int db;

        public Verem(int meret)
        {
            elemek = new string[meret];
            db = 0;
        }

        // új elem betétele a verem "tetejére"
        public void Push(string elem)
        {
            if (db == elemek.Length)
                throw new NincsTobbHelyException();

            elemek[db++] = elem;
        }

        // elem kivétele a verem "tetejéről"
        public string Pop()
        {
            if (db == 0)
                throw new UresVeremException();

            string vissza = elemek[--db];
            return vissza;
        }
    }
}
