﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_07_1_Verem
{
    class Program
    {
        static void Main(string[] args)
        {
            Verem v = new Verem(5);
            
            try
            {
                v.Push("A");
                v.Push("B");
                v.Push("C");

                Console.WriteLine(v.Pop());
                Console.WriteLine(v.Pop());
                Console.WriteLine(v.Pop());
                Console.WriteLine(v.Pop());
            }
            catch (UresVeremException e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }
    }
}
