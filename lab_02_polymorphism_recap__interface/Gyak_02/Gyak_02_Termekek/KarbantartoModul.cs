﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_02_Termekek
{
    class KarbantartoModul
    {
        IKarbantartando[] karbantartandok;
        int karbantartandoN;

        public KarbantartoModul(int karbantartandoMaxDb)
        {
            this.karbantartandok = new IKarbantartando[karbantartandoMaxDb];
            this.karbantartandoN = 0;
        }

        public void UjKarbantartoFelvetele(IKarbantartando uj)
        {
            if (this.karbantartandoN < karbantartandok.Length)
            {
                this.karbantartandok[karbantartandoN] = uj;
                karbantartandoN++;
            }
        }

        public void MindenKarbantartas()
        {
            for (int i = 0; i < karbantartandoN; i++)
            {
                if (karbantartandok[i].KarbantartasSzukseges())
                    Console.WriteLine(karbantartandok[i].Karbantartas());
            }
        }
    }
}
