﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_02_Termekek
{
    abstract class Termek : IEladhato
    {
        public string Nev { get; set; }

        public abstract int Ar { get; }

        public Termek(string nev)
        {
            Nev = nev;
        }
    }
}
