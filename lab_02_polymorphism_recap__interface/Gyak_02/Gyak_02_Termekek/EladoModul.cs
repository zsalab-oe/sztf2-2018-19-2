﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_02_Termekek
{
    class EladoModul
    {
        IEladhato[] eladhatok;
        int eladhatoN;

        public EladoModul(int eladhatokMaxDb)
        {
            this.eladhatok = new IEladhato[eladhatokMaxDb];
            this.eladhatoN = 0;
        }

        public void UjTermekFelvetele(IEladhato uj)
        {
            if (this.eladhatoN < eladhatok.Length)
            {
                this.eladhatok[eladhatoN] = uj;
                eladhatoN++;
            }
        }

        public IEladhato Legolcsobb()
        {
            if (this.eladhatoN == 0)
                return null;

            int min = 0;
            for (int i = 1; i < eladhatoN; i++)
            {
                if (eladhatok[i].Ar < eladhatok[min].Ar)
                    min = i;
            }
            return eladhatok[min];
        }
    }
}
