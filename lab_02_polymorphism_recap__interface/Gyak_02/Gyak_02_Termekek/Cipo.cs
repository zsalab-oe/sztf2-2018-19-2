﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_02_Termekek
{
    class Cipo : Termek, IVisszavalthato
    {
        int meret;

        public Cipo(int meret)
            : base("Cipő")
        {
            this.meret = meret;
        }

        public override int Ar
        {
            get { return meret > 40 ? 15000 : 14000; }
        }

        public int VisszavaltasiErteke(int elteltNapokSzáma)
        {
            return Math.Max(Ar - Ar * elteltNapokSzáma / 50, 0);
        }
    }
}
