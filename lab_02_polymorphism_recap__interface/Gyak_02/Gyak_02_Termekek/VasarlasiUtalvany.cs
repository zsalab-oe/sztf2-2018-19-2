﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_02_Termekek
{
    class VasarlasiUtalvany : IEladhato, IVisszavalthato
    {
        public int Ar { get { return 1000; } }

        public int VisszavaltasiErteke(int elteltNapokSzáma)
        {
            return 1000;
        }
    }
}
