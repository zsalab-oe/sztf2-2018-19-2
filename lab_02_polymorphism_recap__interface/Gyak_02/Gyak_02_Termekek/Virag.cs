﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_02_Termekek
{
    class Virag : Termek, IKarbantartando
    {
        int kor;

        public Virag(string nev, int kor) : base(nev)
        {
            this.kor = kor;
        }

        public override int Ar
        {
            get { return 1000 + kor * 2; }
        }

        public bool KarbantartasSzukseges()
        {
            kor++;
            return kor % 3 == 0;
        }

        public string Karbantartas()
        {
            return "Öntözés";
        }
    }
}
