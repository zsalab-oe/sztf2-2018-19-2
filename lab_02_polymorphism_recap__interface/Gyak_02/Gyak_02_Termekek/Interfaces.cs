﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_02_Termekek
{
    interface IEladhato
    {
        int Ar { get; }
    }

    interface IKarbantartando
    {
        bool KarbantartasSzukseges();
        string Karbantartas();
    }

    interface IVisszavalthato : IEladhato
    {
        int VisszavaltasiErteke(int elteltNapokSzáma);
    }
}
