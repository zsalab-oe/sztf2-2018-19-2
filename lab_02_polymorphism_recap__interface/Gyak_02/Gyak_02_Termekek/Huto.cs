﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_02_Termekek
{
    class Huto : IKarbantartando
    {
        public string Karbantartas()
        {
            return "Tisztítás";
        }

        public bool KarbantartasSzukseges()
        {
            return true;
        }
    }
}
