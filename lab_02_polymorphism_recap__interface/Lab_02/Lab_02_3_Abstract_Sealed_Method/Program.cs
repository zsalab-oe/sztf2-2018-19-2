﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02_3_Abstract_Sealed_Method
{

    //// az osztályokat átraktam egy fájlba (mivel csak pár sorosak), így talán jobban átlátható, ha egy helyen vannak



    abstract class Jatekos // egy osztály kötelezően absztrakt, ha legalább egy absztrakt metódusa van
    {
        // ha itt még nem tudjuk implementálni a metódust, megjelölhetjük absztraktként
        // nincs metódustörzs!
        
        public abstract void Koszon();
    }

    class Kapus : Jatekos
    {
        public override void Koszon() { Console.WriteLine("Szia, kapus vagyok."); } // absztrakt metódusokat a leszármazottakban kötelező implementálni (vagy absztraktként jelölni)
    }

    abstract class Tamado : Jatekos { }

    class Csatar : Tamado
    {
        public sealed override void Koszon() { Console.WriteLine("Szia, csatár vagyok"); }
    }

    class KetBallabasCsatar : Csatar
    {
        //public override void Koszon() { Console.WriteLine("Szia, tulajdonképpen én is csatár vagyok."); } // Hibás, lezárt metódust nem lehet felülírni
    }

    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
