﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02_1_Polimorfizmus
{
    class Csatar : Tamado
    {
        public new void Bemutatkozas()
        {
            Console.WriteLine("Szia, csatár vagyok.");
        }

        public override void Leiras()
        {
            Console.WriteLine("Bevarrom a léc alá.");
        }
    }
}
