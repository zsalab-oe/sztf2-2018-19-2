﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02_1_Polimorfizmus
{
    class Jatekos
    {
        public void Bemutatkozas()
        {
            Console.WriteLine("Szia, football játékos vagyok.");
        }

        public virtual void Leiras()
        {
            Console.WriteLine("Szaladgálok a gyepen.");
        }
    }
}
