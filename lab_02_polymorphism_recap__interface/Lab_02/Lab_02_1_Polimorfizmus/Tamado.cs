﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02_1_Polimorfizmus
{
    class Tamado : Jatekos
    {
        public new void Bemutatkozas()
        {
            Console.WriteLine("Szia, támadó vagyok.");
        }

        public override void Leiras()
        {
            Console.WriteLine("Támadok.");
        }
    }
}
