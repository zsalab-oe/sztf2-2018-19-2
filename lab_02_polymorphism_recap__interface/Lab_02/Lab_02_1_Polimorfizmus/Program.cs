﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02_1_Polimorfizmus
{
    class Program
    {
        static void Main(string[] args)
        {
            Csatar csatar = new Csatar();
            csatar.Bemutatkozas();

            Jatekos[] csapat = new Jatekos[]
            {
                new Tamado(),
                new Csatar(),
                new Kapus(),
                new Jatekos()
            };

            // =======================================================================================
            // Polimorfizmus nem-virtuális metódussal? Így nincs polimorfizmus!
            // lefuttatva látható, hogy mindenkinél a Jatekos osztályban létrehozott metódus fut le

            Console.WriteLine("\n-------------");
            for (int i = 0; i < csapat.Length; i++)
            {
                csapat[i].Bemutatkozas();
            }

            // =======================================================================================
            // Polimorfizmus virtuális metódussal

            Console.WriteLine("\n-------------");
            for (int i = 0; i < csapat.Length; i++)
            {
                csapat[i].Leiras();
            }
        }
    }
}
