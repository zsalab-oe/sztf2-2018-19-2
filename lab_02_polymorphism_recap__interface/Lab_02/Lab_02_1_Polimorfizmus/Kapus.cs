﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02_1_Polimorfizmus
{
    class Kapus : Jatekos
    {
        public new void Bemutatkozas()
        {
            Console.WriteLine("Szia, kapus vagyok.");
        }

        public override void Leiras()
        {
            Console.WriteLine("Őrzöm a hálót.");
        }
    }
}
