﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02_5_Interface_Allat
{
    interface IRepul
    {
        // láthatósági jelzőket nem kell, és nem is lehet megadni (minden public)
        int Szarnytav { get; }
        void Repul();
    }
}
