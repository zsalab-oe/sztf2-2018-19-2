﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02_5_Interface_Allat
{
    class Program
    {
        static void Main(string[] args)
        {
            Kutya b = new Kutya("Buksi", 5);
            Macska c = new Macska("Cirmi", 4);
            Galamb g = new Galamb("Galamb Gabi", 2, 40);
            Denever d = new Denever("Denever Dani", 10, 20);

            Allat[] allatok = new Allat[] { b, c, g, d };
            IRepul[] repulok = new IRepul[] { g, d };
            IKommunikal[] beszelok = new IKommunikal[] { b, c, g, d };

            // ================================================================================
            // polimorfizmus működik interfészek esetén is
            // hiszen ez esetben CSAK késői kötéssel működnek a metódusok
            // ha belegondolunk, korai kötés nem is működne, hiszen nincs az "ősben" (interfészben) kifejtve a metódus

            foreach (IKommunikal beszelo in beszelok)
            {
                beszelo.Beszel();
            }

            Console.WriteLine("\n----------------");
            foreach (IRepul r in repulok)
            {
                r.Repul();
            }

            Console.WriteLine("\n-------- CompareTo() --------");
            Console.WriteLine(b.CompareTo(d)); // b-hez hasonlítom d-t

            // ================================================================================
            // Miért jó még a CompareTo?
            // => Beépített függvények is ezt használják, pl.: Array.Sort()

            Console.WriteLine("\n>> Lista:");
            for (int i = 0; i < allatok.Length; i++)
            {
                Console.WriteLine(allatok[i].Nev + " " + allatok[i].Eletkor);
            }

            Array.Sort(allatok);

            Console.WriteLine("\n>> Rendezett lista:");
            for (int i = 0; i < allatok.Length; i++)
            {
                Console.WriteLine(allatok[i].Nev + " " + allatok[i].Eletkor);
            }
        }
    }
}
