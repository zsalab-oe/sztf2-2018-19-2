﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02_5_Interface_Allat
{
    abstract class Madar : Allat, IRepul // Fontos a sorrend!
    {
        int szarnytav;
        public int Szarnytav
        {
            get { return szarnytav; }
        }
        //public int Szarnytav { get; private set; } // ez ugyanaz
        public Madar(string nev, int eletkor, int szarnytav) : base(nev, eletkor)
        {
            this.szarnytav = szarnytav;
        }

        public void Repul()
        {
            Console.WriteLine("Repülök.");
        }

        public override void Beszel()
        {
            Console.WriteLine("csip-csirip");
        }
    }
}
