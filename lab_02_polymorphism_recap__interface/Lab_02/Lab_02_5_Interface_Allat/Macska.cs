﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02_5_Interface_Allat
{
    class Macska : Emlos
    {
        public Macska(string nev, int eletkor) : base(nev, eletkor)
        {
        }

        public override void Beszel()
        {
            Console.WriteLine("miau");
        }
    }
}
