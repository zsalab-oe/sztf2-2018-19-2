﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02_5_Interface_Allat
{
    class Denever : Emlos, IRepul
    {
        public int Szarnytav { get; private set; }

        public Denever(string nev, int eletkor, int szarnytav)
            : base(nev, eletkor)
        {
            this.Szarnytav = szarnytav;
        }

        public override void Beszel()
        {
            Console.WriteLine("denevérhang");
        }

        public void Repul()
        {
            Console.WriteLine("Repülök, és denevér vagyok");
        }
    }
}
