﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02_5_Interface_Allat
{
    // egy osztály egyszerre több interfészt is megvalósíthat
    // absztrakt osztály is megvalósíthat interfészt (ilyenkor a metódust nem szükséges implementálni, azonban kötelezően absztraktként kell megjelölni)
    // többszörös öröklődés (-szerű!) is megvalósítható interfészekkel
    abstract class Allat : IKommunikal, IComparable
    {
        public string Nev { get; set; }
        public int Eletkor { get; set; }
        public string Szamlaszam { get; set; }
        public int Egyenleg { get; set; }

        protected Allat(string nev, int eletkor)
        {
            Nev = nev;
            Eletkor = eletkor;
        }

        // IKommunikal interfész miatt kell
        public abstract void Beszel();


        // IComparable interfész miatt kell
        public int CompareTo(object obj)
        {
            // CompareTo-n belül ÉN tudom megmondani, hogy MI ALAPJÁN legyen az összehasonlítás!

            Allat masik = (Allat)obj;
            if (this.Eletkor < masik.Eletkor)
                return -1;
            if (this.Eletkor > masik.Eletkor)
                return 1;
            else
                return 0;
        }
    }
}
