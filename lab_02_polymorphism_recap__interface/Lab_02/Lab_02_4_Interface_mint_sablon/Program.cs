﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02_4_Interface_mint_sablon
{
    interface IJatekos
    {
        // láthatósági jelzőket nem kell, és nem is lehet megadni (minden public)
        void Koszon();
    }
    class Kapus : IJatekos
    {
        public void Koszon() { Console.WriteLine("Szia, kapus vagyok."); }
    }
    class Tamado : IJatekos
    {
        public void Koszon()
        {
            Console.WriteLine("Szia, támadó vagyok.");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Tamado messi = new Tamado();
            Tamado eriksen = new Tamado();
            Kapus navas = new Kapus();

            IJatekos ronaldo = new Tamado(); // interfész használható referencia típusként
            //Kapus dibusz = IJatekos(); // hibás, interfész típus nem példányosítható

            IJatekos[] csapat = new IJatekos[] { messi, eriksen, navas };

            foreach (IJatekos jatekos in csapat)
            {
                jatekos.Koszon();
            }
        }
    }
}
