﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02_2_Abstract_Sealed_Class
{
    abstract class Ember { }
    abstract class Jatekos : Ember { }
    class Kapus : Jatekos { }
    class Tamado : Jatekos { }
    sealed class Csatar : Tamado { }

    //class KetBallabasCsatar : Csatar { } // Hibás, lezárt osztályból nem engedi a származtatást

    class Program
    {
        static void Main(string[] args)
        {
            //Ember e = new Ember(); // Hibás, absztakt osztályból nem lehet példányosítani
            //Jatekos j = new Jatekos(); // szintén
            Tamado t = new Tamado(); // OK
            Csatar cs = new Csatar(); // OK
            Ember tesztRef = t;

            // Lényegében:
            // abstract class >> ne lehessen példányt létrehozni belőle
            // sealed class >> ne lehessen származtatni belőle
        }
    }
}
