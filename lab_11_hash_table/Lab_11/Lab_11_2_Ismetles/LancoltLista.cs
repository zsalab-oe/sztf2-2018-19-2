﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_11_2_Ismetles
{
    class LancoltLista<K,T> : IEnumerable<T> where K : IComparable
    {
        class ListaElem
        {
            public K kulcs;
            public T tart;
            public ListaElem kov;
        }

        class ListaBejaro : IEnumerator<T>
        {
            ListaElem elso, jelenlegi;

            public ListaBejaro(ListaElem elso)
            {
                this.elso = elso;
            }
            public T Current { get { return jelenlegi.tart; } }

            object IEnumerator.Current { get { return this.Current; } }

            public void Dispose()
            {
                //elso = null;
                //jelenlegi = null;
            }

            public bool MoveNext()
            {
                if (jelenlegi == null)
                    jelenlegi = elso;
                else
                    jelenlegi = jelenlegi.kov;

                return jelenlegi != null;
            }

            public void Reset()
            {
                //jelenlegi = null;
            }
        }

        ListaElem fej;

        public void Beszuras(K kulcs, T ertek)
        {
            ListaElem uj = new ListaElem();
            uj.kulcs = kulcs;
            uj.tart = ertek;
            ListaElem p = fej;
            ListaElem e = null;
            while (p != null && p.kulcs.CompareTo(kulcs) < 0)
            {
                e = p;
                p = p.kov;
            }
            if (e == null)
            {
                uj.kov = fej;
                fej = uj;
            }
            else
            {
                uj.kov = p;
                e.kov = uj;
            }
        }

        public LancoltLista<K,T> Unio(LancoltLista<K,T> masik)
        {
            LancoltLista<K, T> l = new LancoltLista<K, T>();
            // TODO :)
            
            return l;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new ListaBejaro(fej);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
