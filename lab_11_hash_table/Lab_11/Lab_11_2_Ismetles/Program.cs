﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_11_2_Ismetles
{
    class Program
    {
        static void Main(string[] args)
        {
            LancoltLista<int, string> l = new LancoltLista<int, string>();
            l.Beszuras(3, "citrom");
            l.Beszuras(5, "eper");
            l.Beszuras(1, "alma");
            l.Beszuras(2, "banán");
            l.Beszuras(4, "dinnye");

            foreach (string e in l)
                Console.WriteLine(e);

            Console.ReadLine();
        }
    }
}
