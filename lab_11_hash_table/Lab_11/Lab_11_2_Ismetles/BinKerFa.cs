﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_11_2_Ismetles
{
    class BinKerFa<K,T> : IEnumerable<T> where K : IComparable
    {
        class FaElem
        {
            public K kulcs;
            public T tart;
            public FaElem bal, jobb;
        }

        FaElem gyoker;



        void InOrder(FaElem p, LancoltLista<K,T> l)
        {
            if (p != null)
            {
                InOrder(p.bal, l);
                l.Beszuras(p.kulcs, p.tart);
                InOrder(p.jobb, l);
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            LancoltLista<K, T> l = new LancoltLista<K, T>();
            InOrder(gyoker, l);
            return l.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
