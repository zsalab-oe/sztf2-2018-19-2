﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_11_1_Hasito_Tablazat
{
    abstract class HasitoTablazat<K,T>
    {
        protected int m;

        protected HasitoTablazat(int m)
        {
            this.m = m;
        }

        protected virtual int h(K kulcs)
        {
            return Math.Abs(kulcs.GetHashCode() % m);
        }

        public abstract void Beszuras(K kulcs, T ertek);
        public abstract T Kereses(K kulcs);
        public abstract void Torles(K kulcs);
    }
}
