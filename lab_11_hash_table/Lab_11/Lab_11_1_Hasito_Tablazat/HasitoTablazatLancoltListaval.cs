﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_11_1_Hasito_Tablazat
{
    class NincsIlyenKulcsHasitoKivetel : Exception { }
    class HasitoTablazatLancoltListaval<K, T> : HasitoTablazat<K,T>
    {
        class HasitoElem
        {
            public K kulcs;
            public T tart;
            public HasitoElem kov;
        }

        HasitoElem[] A;

        public HasitoTablazatLancoltListaval(int m) : base(m)
        {
            A = new HasitoElem[m];
        }

        public override void Beszuras(K kulcs, T ertek)
        {
            HasitoElem uj = new HasitoElem();
            uj.kulcs = kulcs;
            uj.tart = ertek;
            uj.kov = A[h(kulcs)];
            A[h(kulcs)] = uj;
        }

        public override T Kereses(K kulcs)
        {
            HasitoElem p = A[h(kulcs)];
            while (p != null && !p.kulcs.Equals(kulcs))
                p = p.kov;
            if (p != null)
                return p.tart;
            else
                throw new NincsIlyenKulcsHasitoKivetel();
        }

        public override void Torles(K kulcs)
        {
            HasitoElem p = A[h(kulcs)];
            HasitoElem e = null;
            while (p != null && !p.kulcs.Equals(kulcs))
            {
                e = p;
                p = p.kov;
            }
            if (p != null)
            {
                if (e == null)
                    A[h(kulcs)] = p.kov;
                else
                    e.kov = p.kov;
            }
            else
                throw new NincsIlyenKulcsHasitoKivetel();
        }
    }
}
