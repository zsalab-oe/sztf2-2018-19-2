﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_11_1_Hasito_Tablazat
{
    class Gyumolcs
    {
        public string Nev { get; }
        public Gyumolcs(string nev)
        {
            Nev = nev;
        }

        public override int GetHashCode()
        {
            return Nev.GetHashCode();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            HasitoTeszt_1();

            Console.WriteLine("-------------------");

            HasitoTeszt_2();

            Console.WriteLine("-------------------");
            
            DictionaryTeszt();

            Console.ReadLine();
        }

        static void HasitoTeszt_1()
        {
            HasitoTablazatLancoltListaval<string, int> ht = new HasitoTablazatLancoltListaval<string, int>(31);

            ht.Beszuras("alma", 5);
            ht.Beszuras("banán", 10);
            ht.Beszuras("citrom", 2);
            ht.Beszuras("dinnye", 2);
            ht.Beszuras("eper", 10);

            Console.WriteLine(ht.Kereses("eper"));
            ht.Torles("citrom");
            try
            {
                Console.WriteLine(ht.Kereses("citrom"));
            }
            catch (NincsIlyenKulcsHasitoKivetel)
            {
                Console.WriteLine("Nincs ilyen elem.");
            }
        }

        static void HasitoTeszt_2()
        {
            HasitoTablazatLancoltListaval<Gyumolcs, int> ht = new HasitoTablazatLancoltListaval<Gyumolcs, int>(31);
            Gyumolcs a = new Gyumolcs("alma");
            Gyumolcs b = new Gyumolcs("banán");
            Gyumolcs c = new Gyumolcs("citrom");
            Gyumolcs d = new Gyumolcs("dinnye");
            Gyumolcs e = new Gyumolcs("eper");

            ht.Beszuras(a, 5);
            ht.Beszuras(b, 10);
            ht.Beszuras(c, 2);
            ht.Beszuras(d, 2);
            ht.Beszuras(e, 10);

            Console.WriteLine(ht.Kereses(e));
            ht.Torles(c);
            try
            {
                Console.WriteLine(ht.Kereses(c));
            }
            catch (NincsIlyenKulcsHasitoKivetel)
            {
                Console.WriteLine("Nincs ilyen elem.");
            }
        }

        static void DictionaryTeszt()
        {
            // Ugyanaz a példa a beépített Dictionary osztállyal:

            Dictionary<Gyumolcs, int> ht = new Dictionary<Gyumolcs, int>();
            Gyumolcs a = new Gyumolcs("alma");
            Gyumolcs b = new Gyumolcs("banán");
            Gyumolcs c = new Gyumolcs("citrom");
            Gyumolcs d = new Gyumolcs("dinnye");
            Gyumolcs e = new Gyumolcs("eper");

            ht.Add(a, 5);
            ht.Add(b, 10);
            ht.Add(c, 2);
            ht.Add(d, 2);
            ht.Add(e, 10);

            Console.WriteLine(ht[e]);
            ht.Remove(c);
            try
            {
                Console.WriteLine(ht[c]);
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Nincs ilyen elem.");
            }
        }
    }
}
