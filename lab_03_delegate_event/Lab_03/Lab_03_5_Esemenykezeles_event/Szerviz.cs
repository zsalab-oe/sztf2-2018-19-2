﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_5_Esemenykezeles_event
{
    // ========================================
    // 3 lépés:
    //     1. delegált definiálása
    //     2. esemény a delegált alapján
    //     3. esemény elsütése, ahol szükséges
    // ========================================


    // delegált definiálása
    delegate void JavitasEventHandler(Auto auto);

    class Szerviz
    {
        // esemény a delegált alapján
        public event JavitasEventHandler javitva;
        // a háttérben az 'event' kiváltja a fel- és leiratkozást, valamint, hogy private-ként vesszük a delegáltat!

        public void Javitas(Auto auto)
        {
            Console.WriteLine("A javítás megkezdve...");

            System.Threading.Thread.Sleep(2000); // hogy szimbolizáljuk a javítás idejét...

            OnJavitva(auto);
        }

        void OnJavitva(Auto auto) // esemény elsütése
        {
            if (javitva != null) // vizsgálat azért kell, hogy ha nincs semmi sem feliratkozva, ne haljon meg
                javitva(auto);
        }
    }
}
