﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_5_Esemenykezeles_event
{
    class MailService
    {
        public void Ertesites(Auto auto)
        {
            Console.WriteLine("[MAIL] - A(z) " + auto.Rendszam + " rendszámú autó javítása elkészült, lehet érte jönni.");
        }
    }

    class FacebookService
    {
        public void Ertesites(Auto auto)
        {
            Console.WriteLine("[FACEBOOK] - A(z) " + auto.Rendszam + " rendszámú autó javítása elkészült, lehet érte jönni.");
        }
    }



    class Program
    {
        static void Main(string[] args)
        {
            Auto auto = new Auto("ABC-123");
            Szerviz szerviz = new Szerviz();  // eseményt küldő (publisher)

            MailService mail = new MailService(); // eseményt feldolgozó (subscriber)
            FacebookService facebook = new FacebookService(); // eseményt feldolgozó (subscriber)

            // feliratkoztatás, "gyűjtőbe belerakás"
            szerviz.javitva += mail.Ertesites;
            szerviz.javitva += facebook.Ertesites;

            szerviz.Javitas(auto);
            
            // hibásnak jelezné, event használatával innen kívülről NEM hívhatom meg tetszőlegesen, ezzel kiküszöbölve a helytelen működést
            //szerviz.javitva(auto);

            Console.WriteLine("\n----------------");

            // leiratkoztatás, "gyűjtőből kivétel"
            szerviz.javitva -= mail.Ertesites;

            szerviz.Javitas(auto);

            Console.WriteLine("----------------");

            szerviz.javitva -= facebook.Ertesites;

            szerviz.Javitas(auto);
        }
    }
}