﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_5_Esemenykezeles_event
{
    class Auto
    {
        public string Rendszam { get; set; }

        public Auto(string rendszam)
        {
            Rendszam = rendszam;
        }
    }
}
