﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_3_Esemenykezeles_delegate_2
{
    // delegált definiálása
    delegate void SzovegFeldolgozo(string szoveg);

    static class Muveletek
    {
        public static void KiirKicsi(string szoveg)
        {
            Console.WriteLine(szoveg.ToLower());
        }

        public static void KiirNagy(string szoveg)
        {
            Console.WriteLine(szoveg.ToUpper());
        }

        public static int NemKompatibilisMetodus(int a, int b)
        {
            return a + b;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // Metódushívás képviselőn keresztül:

            // - példányt ebből is lehet létrehozni
            // - ehhez a konstruktorának át kell adni a későbbiekben hívandó metódust
            // - figyeljünk, hogy az átadott metódus (és a később feliratkoztatott metódusok) mind egyforma szignatúrával kell rendelkezzenek!
            // - ezt a szignatúrát a delegált (képviselő) típusa írja elő /amit fent adtunk meg/

            SzovegFeldolgozo feldolgozo = new SzovegFeldolgozo(Muveletek.KiirKicsi); // feliratkoztatás, "gyűjtőbe belerakás"

            feldolgozo += Muveletek.KiirNagy; // feliratkoztatás, "gyűjtőbe belerakás"
            //feldolgozo += new SzovegFeldolgozo(Muveletek.KiirNagy); // így is lehet írni

            //feldolgozo += Muveletek.NemKompatibilisMetodus; // erre már problémázna, mert a szignatúra nem egyezik meg


            feldolgozo("Teszt Üzenet");
            // - lefuttatva látható, hogy tulajdonképpen minden metódust "lefuttathatunk", amelyet előzetesen a "gyűjtőbe" belerakunk

            Console.WriteLine("\n--------------------\n");

            feldolgozo -= Muveletek.KiirNagy; // leiratkoztatás, "gyűjtőből kivétel"

            feldolgozo("Teszt Üzenet");
        }
    }
}
