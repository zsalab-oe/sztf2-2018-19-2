﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_1_Esemenykezeles_interface
{
    interface IHivasFigyelo
    {
        void BejovoHivasTortent(Telefon kuldo, string forras_telefonszam);
        void KimenoHivasTortent(Telefon kuldo, string cel_telefonszam);
    }
}
