﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_1_Esemenykezeles_interface
{
    class Telefon
    {
        const int HIVASDIJ = 20;

        public string Telefonszam { get; private set; }
        int egyenleg;

        IHivasFigyelo hivasFigyelo;

        public Telefon(string telefonszam)
        {
            Telefonszam = telefonszam;
            this.egyenleg = 0;
        }

        public void EgyenlegFeltoltes(int osszeg)
        {
            egyenleg += osszeg;
        }

        public void FigyelRegisztralas(IHivasFigyelo hivasFigyelo)
        {
            this.hivasFigyelo = hivasFigyelo;
        }

        public void HivasKezdemenyezes(Telefon cel)
        {
            if (hivasFigyelo != null)
                hivasFigyelo.KimenoHivasTortent(this, cel.Telefonszam); // jelzés az "eseményről" (történt valami)

            if (egyenleg >= HIVASDIJ)
            {
                cel.HivasFogadas(this);
                egyenleg -= HIVASDIJ;
            }
        }

        void HivasFogadas(Telefon forras)
        {
            if (hivasFigyelo != null)
                hivasFigyelo.BejovoHivasTortent(this, forras.Telefonszam);  // jelzés az "eseményről" (történt valami)
        }
    }
}
