﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_1_Esemenykezeles_interface
{
    class Program
    {
        static void Main(string[] args)
        {
            Telefon t1 = new Telefon("1234567");
            Telefon t2 = new Telefon("8585855");
            HivasNaplo naplo = new HivasNaplo(); // "esemény" -kezelő objektum

            t1.FigyelRegisztralas(naplo);
            t2.FigyelRegisztralas(naplo);

            t1.EgyenlegFeltoltes(50);
            t1.HivasKezdemenyezes(t2);

            Console.WriteLine();
            t1.HivasKezdemenyezes(t2);

            Console.WriteLine();
            t2.HivasKezdemenyezes(t1);

            Console.WriteLine();
            t1.HivasKezdemenyezes(t2);
        }
    }
}
