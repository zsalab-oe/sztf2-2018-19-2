﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_1_Esemenykezeles_interface
{
    // "esemény" -kezelő osztály
    // Ez gyakorlatilag csak azért kell, hogy az interface problémáját - miszerint nem lehet belőle példányt létrehozni - ki tudjuk kerülni.
    class HivasNaplo : IHivasFigyelo
    {
        // küldő: aki az ESEMÉNYT küldte (akit épp felhívtak)
        public void BejovoHivasTortent(Telefon kuldo, string forras_telefonszam)
        {
            Console.WriteLine(kuldo.Telefonszam + "-ot hívta " + forras_telefonszam);
        }

        // küldő: aki az ESEMÉNYT küldte (aki kezdeményezte a hívást)
        public void KimenoHivasTortent(Telefon kuldo, string cel_telefonszam)
        {
            Console.WriteLine(kuldo.Telefonszam + " hívta " + cel_telefonszam + "-ot");
        }
    }
}
