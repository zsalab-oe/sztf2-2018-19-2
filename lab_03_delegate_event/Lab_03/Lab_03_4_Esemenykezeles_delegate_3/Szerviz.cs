﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_4_Esemenykezeles_delegate_3
{
    // delegált definiálása
    delegate void JavitasEventHandler(Auto auto);

    class Szerviz
    {
        // Helytelen megoldás: publikus delegált használata
        //public JavitasEventHandler javitva;


        // Helyes megoldás (ettől): privát delegált + feliratkozás és leiratkozás metódusok használata
        JavitasEventHandler javitva;

        public void JavitvaFeliratkozas(JavitasEventHandler feliratkozo)
        {
            javitva += feliratkozo; // feliratkoztatás, "gyűjtőbe belerakás"
        }

        public void JavitvaLeiratkozas(JavitasEventHandler feliratkozo)
        {
            javitva -= feliratkozo; // leiratkoztatás, "gyűjtőből kivétel"
        }
        // -------- Helyes megoldás (eddig)

        
        
        public void Javitas(Auto auto)
        {
            Console.WriteLine("A javítás elkezdődött...");

            // valamit csinálunk...

            JavitasKesz(auto);
        }

        void JavitasKesz(Auto auto)
        {
            // visszajelzés
            if (javitva != null) // vizsgálat azért kell, hogy ha nincs semmi sem feliratkozva, ne haljon meg
                javitva(auto);

            // Itt hívom meg a "gyűjtőt", amiben minden metódus benne van. Ahogy a sima delegáltas példában láttuk, minden metódust meghív az adott paraméterrel.
        }
    }
}
