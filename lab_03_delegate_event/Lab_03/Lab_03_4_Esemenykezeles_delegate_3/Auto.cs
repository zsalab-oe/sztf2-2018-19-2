﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_4_Esemenykezeles_delegate_3
{
    class Auto
    {
        public string Rendszam { get; set; }

        public Auto(string rendszam)
        {
            Rendszam = rendszam;
        }
    }
}
