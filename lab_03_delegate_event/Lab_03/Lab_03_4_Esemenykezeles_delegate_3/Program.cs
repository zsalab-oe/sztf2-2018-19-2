﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_4_Esemenykezeles_delegate_3
{
    // A Szerviz és Auto osztályokat megírtuk 5 éve, vagy megírták nekünk másik dolgozók egy másik cégben.
    // Mi azokhoz nem férünk hozzá, de ettől függetlenül szeretnénk, hogy a javítást elvégezve értesítsenek minket több platformon is.
    // Ha a facebook és a sima email mellé később szeretnénk twitter, vagy bármi egyéb új dolgot integrálni, gond nélkül meg tudjuk tenni!

    class MailService
    {
        public void Ertesites(Auto auto)
        {
            Console.WriteLine("[MAIL] - A(z) " + auto.Rendszam + " rendszámú autó javítása elkészült, lehet érte jönni.");
        }
    }

    class FacebookService
    {
        public void Ertesites(Auto auto)
        {
            Console.WriteLine("[FACEBOOK] - A(z) " + auto.Rendszam + " rendszámú autó javítása elkészült, lehet érte jönni.");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Auto auto = new Auto("ABC-123");
            Szerviz szerviz = new Szerviz();

            MailService mail = new MailService();
            FacebookService facebook = new FacebookService();

            //=======================================================================================================
            // Helytelen megoldás: publikus delegált használata

            // feliratkoztatás, "gyűjtőbe belerakás"
            //szerviz.javitva += mail.Ertesites;
            //szerviz.javitva += facebook.Ertesites;

            // Probléma, ha publikus a delegált, mert ez esetben innen kívülről meghívhatom tetszőlegesen, ezáltal fals programot készítve. Pl.:

            //szerviz.javitva(auto);

            // Ekkor minden feliratkozott metódus lefut. (Kapunk értesítést a javítás befejezéséről, holott még el se küldtük javításra az autót!)


            //=======================================================================================================
            // Helyes megoldás: privát delegált + feliratkozás és leiratkozás metódusok használata
            
            szerviz.JavitvaFeliratkozas(mail.Ertesites);
            szerviz.JavitvaFeliratkozas(facebook.Ertesites);

            szerviz.Javitas(auto);
        }
    }
}
