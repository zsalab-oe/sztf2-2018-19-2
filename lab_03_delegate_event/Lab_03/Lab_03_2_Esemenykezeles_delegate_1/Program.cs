﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_2_Esemenykezeles_delegate_1
{
    // delegált definiálása
    delegate double Kozvetito(double szam);

    static class Muveletek
    {
        public static double Ketszerez(double szam)
        {
            return 2 * szam;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // Metódushívás képviselőn keresztül:

            // - példányt ebből is lehet létrehozni
            // - ehhez a konstruktorának át kell adni a későbbiekben hívandó metódust
            // - a metódus szignatúráját a delegált (képviselő) típusa írja elő /amit fent adtunk meg/

            Kozvetito kozvetito = new Kozvetito(Muveletek.Ketszerez);


            Console.WriteLine(kozvetito(5));
        }
    }
}
