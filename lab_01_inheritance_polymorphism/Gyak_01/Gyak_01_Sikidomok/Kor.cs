﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_01_Sikidomok
{
    class Kor : Sikidom
    {
        public double Sugar { get; set; }

        public Kor(Szin szin, double sugar)
            : base(szin)
        {
            Sugar = sugar;
        }

        public override double Kerulet()
        {
            return 2 * Sugar * Math.PI;
        }

        public override double Terulet()
        {
            return Sugar * Sugar * Math.PI;
        }

        public override string ToString()
        {
            return base.ToString() + "; Sugár: " + Sugar;
        }
    }
}
