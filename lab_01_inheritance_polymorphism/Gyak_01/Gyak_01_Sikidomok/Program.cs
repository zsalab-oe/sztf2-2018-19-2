﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_01_Sikidomok
{
    class Program
    {
        static void Main(string[] args)
        {
            Sikidom[] sikidomok = new Sikidom[5];
            sikidomok[0] = new Teglalap(Szin.kek, 4, 5);
            sikidomok[1] = new Negyzet(Szin.piros, 6, 6);
            sikidomok[2] = new Kor(Szin.sarga, 3);
            sikidomok[3] = new Teglalap(Szin.zold, 2, 8);

            // ==============================================================================
            // 5. síkidom létrehozása

            Console.WriteLine(">> 5. síkidom létrehozása:");
            Console.WriteLine("Add meg a szélességét: ");
            double szelesseg = int.Parse(Console.ReadLine());
            Console.WriteLine("Add meg a magasságát: ");
            double magassag = int.Parse(Console.ReadLine());
            sikidomok[4] = NegyzetVagyTeglalap(Szin.kek, szelesseg, magassag);

            // teszt
            string letrehozott = sikidomok[4] is Negyzet ? "négyzet" : "téglalap";
            Console.WriteLine("Egy " + letrehozott + " létrehozva");

            // ==============================================================================
            // Kilyukasztás

            foreach (Sikidom sikidom in sikidomok)
            {
                LyukasztHaTeruletNagyobbMintKerulet(sikidom);
            }

            // ==============================================================================
            // Listázás

            Console.WriteLine("\n---------------------\n\n>> Listázás:");
            foreach (Sikidom sikidom in sikidomok)
            {
                //Console.WriteLine(sikidom.ToString());
                Console.WriteLine(sikidom); // vagy így
            }

            Console.WriteLine("\nLegnagyobb területű síkidom: " + LegnagyobbTeruletuSikidom(sikidomok));


            Console.ReadLine();
        }

        static Sikidom NegyzetVagyTeglalap(Szin szin, double szelesseg, double magassag)
        {
            if (szelesseg == magassag)
                return new Negyzet(szin, szelesseg, magassag);
            else
                return new Teglalap(szin, szelesseg, magassag);
        }

        static void LyukasztHaTeruletNagyobbMintKerulet(Sikidom vizsgalando)
        {
            if (vizsgalando.Terulet() > vizsgalando.Kerulet())
                vizsgalando.Kilyukaszt();
        }

        static Sikidom LegnagyobbTeruletuSikidom(Sikidom[] elemek)
        {
            int max = 0;
            for (int i = 1; i < elemek.Length; i++)
            {
                if (elemek[i].Terulet() > elemek[max].Terulet())
                    max = i;
            }
            return elemek[max];
        }
    }
}
