﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_01_Sikidomok
{
    class Teglalap : Sikidom
    {
        public double Szelesseg { get; set; }
        public double Magassag { get; set; }

        public Teglalap( Szin szin, double szelesseg, double magassag)
            : base(szin)
        {
            Szelesseg = szelesseg;
            Magassag = magassag;
        }

        public sealed override double Kerulet()
        {
            return 2 * (Szelesseg + Magassag);
        }

        public sealed override double Terulet()
        {
            return Szelesseg * Magassag;
        }

        public override string ToString()
        {
            return base.ToString() + "; Szélesség: " + Szelesseg + "; Magasság: " + Magassag;
        }
    }
}
