﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_01_Sikidomok
{
    enum Szin { kek, piros, zold, sarga }
    abstract class Sikidom
    {
        bool lyukas;
        public Szin Szin { get; private set; }

        public Sikidom(Szin szin)
        {
            this.lyukas = false;
            this.Szin = szin;
        }

        public void Kilyukaszt()
        {
            this.lyukas = true;
        }

        public abstract double Kerulet();
        public abstract double Terulet();

        public override string ToString()
        {
            return Szin + ", " + lyukas + "; Kerülete: " + Kerulet() + "; Területe: " + Terulet();
        }
    }
}
