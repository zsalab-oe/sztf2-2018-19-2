﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_01_Szamlak
{
    class Bank
    {
        Szamla[] szamlak;
        int szamlaN;

        public Bank(int szamlaMaxDb)
        {
            this.szamlak = new Szamla[szamlaMaxDb];
            this.szamlaN = 0;
        }

        public Szamla Szamlanyitas(Tulajdonos tulaj, int hitelkeret)
        {
            if (szamlaN == this.szamlak.Length)
                return null;

            Szamla uj;
            if (hitelkeret > 0)
                uj = new HitelSzamla(tulaj, hitelkeret);
            else
                uj = new MegtakaritasiSzamla(tulaj);
            this.szamlak[szamlaN] = uj;
            szamlaN++;
            return uj;
        }

        public int OsszEgyenleg(Tulajdonos tulaj)
        {
            int ossz = 0;
            for (int i = 0; i < szamlaN; i++)
            {
                if (this.szamlak[i].Tulaj == tulaj)
                    ossz += this.szamlak[i].Egyenleg;
            }
            return ossz;
        }

        public Szamla LegnagyobbEgyenleguSzamla(Tulajdonos tulaj)
        {
            int max = -1;
            for (int i = 0; i < szamlaN; i++)
            {
                if (this.szamlak[i].Tulaj == tulaj && (max == -1 || this.szamlak[i].Egyenleg > this.szamlak[max].Egyenleg))
                {
                    max = i;
                }
            }

            if (max == -1)
                return null;
            //else
                return szamlak[max];
        }

        public int OsszHitelkeret()
        {
            int ossz = 0;
            for (int i = 0; i < szamlaN; i++)
            {
                if (szamlak[i] is HitelSzamla)
                    ossz += (szamlak[i] as HitelSzamla).Hitelkeret;
            }
            return ossz;
        }
    }
}
