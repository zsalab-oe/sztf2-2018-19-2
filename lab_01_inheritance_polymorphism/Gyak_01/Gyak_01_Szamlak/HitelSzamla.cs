﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_01_Szamlak
{
    class HitelSzamla : Szamla
    {
        public int Hitelkeret { get; }
        public HitelSzamla(Tulajdonos tulaj, int hitelkeret) : base(tulaj)
        {
            this.Hitelkeret = hitelkeret;
        }

        public override bool Kivesz(int osszeg)
        {
            if (this.Egyenleg - osszeg >= -this.Hitelkeret)
            {
                this.Egyenleg -= osszeg;
                return true;
            }
            else
                return false;
        }
    }
}
