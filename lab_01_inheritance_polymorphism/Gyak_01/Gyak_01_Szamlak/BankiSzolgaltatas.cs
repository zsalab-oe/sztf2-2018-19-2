﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_01_Szamlak
{
    abstract class BankiSzolgaltatas
    {
        public Tulajdonos Tulaj { get; } // read only (konstruktorban megadható, későbbiekben csak olvasható)

        public BankiSzolgaltatas(Tulajdonos tulaj)
        {
            this.Tulaj = tulaj;
        }
    }
}
