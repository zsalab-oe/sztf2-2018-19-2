﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_01_Szamlak
{
    class Kartya : BankiSzolgaltatas
    {
        public Szamla Szamla { get; }
        public string Kartyaszam { get; private set; }
        public Kartya(Tulajdonos tulaj, Szamla szamla, string kartyaszam) : base(tulaj)
        {
            this.Szamla = szamla;
            this.Kartyaszam = kartyaszam;
        }

        public bool Vasarlas(int osszeg)
        {
            return this.Szamla.Kivesz(osszeg);
        }
    }
}
