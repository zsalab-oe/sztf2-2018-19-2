﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_01_Szamlak
{
    class MegtakaritasiSzamla : Szamla
    {
        static int AlapertelmezettKamat = 5;
        public int Kamat { get; set; }

        public MegtakaritasiSzamla(Tulajdonos tulaj) : base(tulaj)
        {
            this.Kamat = AlapertelmezettKamat;
        }

        public override bool Kivesz(int osszeg)
        {
            if (this.Egyenleg - osszeg >= 0)
            {
                this.Egyenleg -= osszeg;
                return true;
            }
            else
                return false;
        }

        public void KamatJovairas()
        {
            Egyenleg = (int)(Egyenleg * (1 + (double)Kamat / 100));
        }
    }
}
