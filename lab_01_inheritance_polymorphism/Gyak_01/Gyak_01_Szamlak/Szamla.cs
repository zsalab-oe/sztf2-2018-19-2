﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_01_Szamlak
{
    abstract class Szamla : BankiSzolgaltatas
    {
        public int Egyenleg { get; protected set; }
        public Szamla(Tulajdonos tulaj) : base(tulaj)
        {
        }

        public void Befizet(int osszeg)
        {
            this.Egyenleg += osszeg;
        }

        public abstract bool Kivesz(int osszeg);

        public Kartya UjKartya(string kartyaszam)
        {
            return new Kartya(this.Tulaj, this, kartyaszam);
        }
    }
}
