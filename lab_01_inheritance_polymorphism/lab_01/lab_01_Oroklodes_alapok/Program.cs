﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_01_Oroklodes_alapok
{
    class Ember
    {
        public string Nev { get; set; } // string nev; + getter, setter property
        public int Eletkor { get; set; }

        // konstruktor generálás: Ctrl + .
        public Ember(string nev, int eletkor)
        {
            Nev = nev;
            Eletkor = eletkor;
        }

        public virtual void Bemutatkozas()
        {
            Console.WriteLine("{0} vagyok, {1} éves.", this.Nev, this.Eletkor);
        }

        public void Udvozles()
        {
            Console.WriteLine("Szia, ember vagyok");
        }
    }



    class Hallgato : Ember
    {
        public string Neptunkod { get; set; }

        // kötelező meghívni az ősosztály (Ember) konstruktorát még a leszármazott (Hallgato) konstruktorának lefutása előtt --> "base" kulcsszóval
        public Hallgato(string nev, int eletkor, string neptunkod)
            : base(nev, eletkor)
        {
            this.Neptunkod = neptunkod;
        }

        public override void Bemutatkozas()
        {
            Console.WriteLine("{0} ({1}) vagyok, {2} éves", this.Nev, this.Neptunkod, this.Eletkor);
        }

        public new void Udvozles()
        {
            Console.WriteLine("Szia, hallgató vagyok");
        }
    }


    class Oktato : Ember
    {
        public int OktatottOrakSzama { get; set; }

        // kötelező meghívni az ősosztály (Ember) konstruktorát még a leszármazott (Oktato) konstruktorának lefutása előtt --> "base" kulcsszóval
        public Oktato(string nev, int eletkor, int oktatottOrakSzama)
            : base(nev, eletkor)
        {
            OktatottOrakSzama = oktatottOrakSzama;
        }

        public override void Bemutatkozas()
        {
            Console.WriteLine("{0} vagyok, {1} órát tartok.", this.Nev, this.OktatottOrakSzama);
        }

        public new void Udvozles()
        {
            Console.WriteLine("Szia, oktató vagyok");
        }
    }



    class Program
    {
        static void Main(string[] args)
        {
            Ember ember = new Ember("Pista", 20);
            Console.WriteLine(ember.Eletkor);

            Hallgato hallgato;
            hallgato = new Hallgato("Kati", 22, "ABCDEF");
            Console.WriteLine(hallgato.Eletkor);



            // =======================================================================================

            // Típuskompatibilitás, típusátalakítás

            // Minden T típusú objektumra hivatkozhatunk T típusú vagy T bármelyik őse típusú referenciával
            // egyenlőségjel bal oldalán: referencia
            // egyenlőségjel jobb oldalán: objektum

            Ember jozsi = new Hallgato("Józsi", 19, "ASDASD");
            Ember bela = new Oktato("Béla", 23, 4);
            Ember[] szemelyek = new Ember[3];
            szemelyek[0] = new Hallgato("Sanyi", 24, "QWEQWE");
            szemelyek[1] = new Hallgato("Feri", 24, "FJFGKJD");
            szemelyek[2] = new Oktato("Laci", 38, 4);


            // Probléma: az ős referenciáján keresztül a leszármazottakban felvett új mezők és metódusok nem érhetők el (pl.: jozsi.Neptunkod)

            // =======================================================================================
            // 1. megoldás: Kasztolás
            // nem jó, mert a tömbben a nem Hallgato objektumokat nem tudjuk átalakítani (InvalidCastException)

            //for (int i = 0; i < szemelyek.Length; i++)
            //{
            //    Console.WriteLine(((Hallgato)szemelyek[i]).Neptunkod);
            //}

            // =======================================================================================
            // 2. megoldás: as operátor
            // ez sem jó, ez nullt ad vissza, ha nem sikerült az átalakítás (NullReferenceException a nem Hallgato objektumoknál)

            //for (int i = 0; i < szemelyek.Length; i++)
            //{
            //    Console.WriteLine((szemelyek[i] as Hallgato).Neptunkod);
            //}

            // =======================================================================================
            // 3. megoldás: is & as operátor
            // ez már lefut, de sok leszármazott esetén sok elágazás

            Console.WriteLine("\n----------------\n");
            for (int i = 0; i < szemelyek.Length; i++)
            {
                if (szemelyek[i] is Hallgato)
                    Console.WriteLine((szemelyek[i] as Hallgato).Neptunkod);
                //if (szemelyek[i] is Oktato)
                //    Console.WriteLine((szemelyek[i] as Oktato).OktatottOrakSzama);
            }

            // =======================================================================================
            // Polimorfizmus virtuális metódussal

            Console.WriteLine("\n-------- Bemutatkozások (virtuális): --------\n");
            for (int i = 0; i < szemelyek.Length; i++)
            {
                szemelyek[i].Bemutatkozas();
            }

            // =======================================================================================
            // Polimorfizmus nem-virtuális metódussal? Így nincs polimorfizmus!
            // lefuttatva látható, hogy mindenkinél az Ember osztályban létrehozott metódus fut le

            Console.WriteLine("\n-------- Üdvözlések (nem-virtuális): --------\n");
            for (int i = 0; i < szemelyek.Length; i++)
            {
                szemelyek[i].Udvozles();
            }

            // =======================================================================================
            // Az előző probléma megkerülése
            // nem szép megoldás, helyette használjunk virtuális metódusokat

            Console.WriteLine("\n----------------\n");
            for (int i = 0; i < szemelyek.Length; i++)
            {
                if (szemelyek[i] is Hallgato)
                    (szemelyek[i] as Hallgato).Udvozles();
                if (szemelyek[i] is Oktato)
                    (szemelyek[i] as Oktato).Udvozles();
            }

            // =======================================================================================
            // Kiegészítés: nem-virtuális metódusok használata

            Console.WriteLine("\n----------------\n");
            Ember e = new Ember("Géza", 47);
            e.Udvozles();

            // a Hallgato osztályban a "new" kulcsszóval elrejtettük az ős metódusát (elrejtés ~ felüldefiniálás)
            // különbség az előzőekhez képest: itt a referencia és az objektum típusa megegyezik (nincs szükség típuskonverzióra)
            Hallgato h = new Hallgato("Bence", 20, "QW2XIM");
            h.Udvozles();



            Console.ReadLine();
        }
    }
}
