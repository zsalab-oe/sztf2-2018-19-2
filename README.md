# Szoftvertervezés és -fejlesztés II.
--------------

A Szoftvertervezés és -fejlesztés II. laborokon ismertetett anyagok kódjai és példái.

## Letöltés

* .zip letöltés: Downloads >> Download repository
* cli: `git clone https://zsalab-oe@bitbucket.org/zsalab/sztf2.git`

## Hasznos linkek

* Az anyagok megtalálhatók a [http://users.nik.uni-obuda.hu/zsalab/](http://users.nik.uni-obuda.hu/zsalab/) oldalamon is.
* Előadás- és labordiák a tantárgy honlapján: [http://users.nik.uni-obuda.hu/sztf2/](http://users.nik.uni-obuda.hu/sztf2/)

## Kapcsolat

Kérdés, óhaj, sóhaj, panasz esetén bátran keressetek a `gaspar.balazs {at} nik.uni-obuda.hu` e-mail címen!

-----------------
*Gáspár Balázs*